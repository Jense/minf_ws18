﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastARCamera : MonoBehaviour {

    private RaycastHit hit;
    private bool hitObject = false;
    private GameObject spinning;

    public GameObject obj;
	// Use this for initialization
	void Start () {
        if (this.gameObject.name.Equals("Cube"))
        {
            spinning = GameObject.Find("small_cubes");
            spinning.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        Transform cam = Camera.main.transform;
        Ray ray = new Ray(cam.position, cam.forward);
        Debug.DrawRay(ray.origin, ray.direction * 1000.0f, Color.green, 10, false);

        if (Physics.Raycast (ray, out hit, 1000.0f))
        {
            Debug.Log("Looking for " + this.transform.name + " hitting " + hit.transform.name);
            if (hit.transform.name == this.transform.name)
            {
                hitObject = true;
            }
            else
            {
                hitObject = false;
            }
        }

        if (hitObject)
        {
            ChangeMaterialColor(this.gameObject, Color.green);
            if (this.gameObject.name.Equals("Cube"))
                spinning.SetActive(true);
        }
        else
        {
            ChangeMaterialColor(this.gameObject, Color.blue);
            if (this.gameObject.name.Equals("Cube"))
                spinning.SetActive(false);
        }
	}

    void ChangeMaterialColor (GameObject target, Color matColor)
    {
        Material materialColored = new Material(Shader.Find("Transparent/Diffuse"));
        materialColored.color = matColor;
        target.GetComponent<Renderer>().material = materialColored;
    }
}
